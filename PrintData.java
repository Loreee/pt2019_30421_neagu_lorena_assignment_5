package PresentationLayer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


/**
 * implements methods that are required for a nice printing of data
 * @author LORENA NEAGU
 *
 */
public class PrintData {

	/**
	 * prints the number of  days of monitored data that appear in the log file
	 * @param numberOfMonitoredDays
	 */
	public static void printMonitoredDays(int numberOfMonitoredDays) {
		 System.out.println("Number of monitored days : " + numberOfMonitoredDays);
		
	}
	
	/**
	 * 	prints the number of times each activity has appeared over the monitoring period

	 * @param countActivities
	 */
	public static void printCountActivities(Map<String, Integer> countActivities) {
		
		for (Map.Entry<String, Integer> mapEntry : countActivities.entrySet()) {
	        String activity = mapEntry.getKey();
	        Integer nb = mapEntry.getValue();
	        System.out.println(activity + " is done " + nb + " times");
		}
	}
	
	/**
	 * prints the number of times each activity has appeared for each day over the monitoring period
	 * @param countActivitiesEachDay
	 */
	public static void printCountActivitiesEachDay(Map<Integer, Map<String, Integer>> countActivitiesEachDay) {
		for (Entry<Integer, Map<String, Integer>> mapEntry : countActivitiesEachDay.entrySet()) {
	        Integer day = mapEntry.getKey();
	        System.out.println("On day " + day + " :");
	        Map<String, Integer> map = mapEntry.getValue();
	        for (Map.Entry<String, Integer> mapEntry1 : map.entrySet()) {
		        String activity = mapEntry1.getKey();
		        Integer nb = mapEntry1.getValue();
		        System.out.println(activity + " is done " + nb + " times");
	        }
		}
	}

	/** 
	 * prints for each line from the file the map containing for each activity the duration recorded on that line
	 * @param computeDurations
	 */
	public static void printComputeDurations(Map<Integer, Map<String, Integer>> computeDurations) {
		for (Entry<Integer, Map<String, Integer>> mapEntry : computeDurations.entrySet()) {
	        Integer entry = mapEntry.getKey();
	        System.out.println("Entry " + entry + " :");
	        Map<String, Integer> map = mapEntry.getValue();
	        for (Map.Entry<String, Integer> mapEntry1 : map.entrySet()) {
		        String activity = mapEntry1.getKey();
		        Integer nb = mapEntry1.getValue();
		        if(nb > 60) 
			        System.out.println(activity + " is done for " + nb/60 + " hours");
		        else
		        	System.out.println(activity + " is done for " + nb + " minutes");
	        }
		}
	}

	/**
	 * prints the duration over the monitoring period of each activity
	 * @param compute
	 */
	public static void printCompute(Map<String, Long> compute) {
		System.out.println("Activity time over the entire monitored period");
		for (Map.Entry<String, Long> mapEntry : compute.entrySet()) {
	        String activity = mapEntry.getKey();
	        Long nb = mapEntry.getValue();
	        if(nb > 60) 
	            System.out.println(activity + " is done for " + nb/60 + " hours");
	        else
	        	System.out.println(activity + " is done for " + nb + " minutes");
		}
	}
	
	/**
	 * prints the activities that have 90% of the monitoring records with duration less than 5 minutes
	 * @param filter
	 */
	public static void printFilter(List<String> filter) {
		System.out.println("Activities that have 90% of the monitoring records with duration less than 5 minutes:");
        for (Iterator<String> it = filter.iterator(); it.hasNext();) {
        	System.out.println(it.next());
        }
	}
}
