package DataLayer;
/**
 * class that contains ass fields the person�s activity as tuples (start_time, end_time, activity_label);
 * start_time, end_time - the date and time when each activity has started and ended while the
 * activity - the type of activity performed by the person
 * @author LORENA NEAGU
 *
 */
public class MonitoredData {
	String start_time;
	String end_time;
	String activity;
	
	public MonitoredData(String start_time, String end_time, String activity){
		this.start_time = start_time;
		this.end_time = end_time;
		this.activity = activity;
	}
	/**
	 * getters for the fields of the class
	 * @return the field as string
	 */
	public String getStart_time() {
		return start_time;
	}

	public String getEnd_time() {
		return end_time;
	}

	public String getActivity() {
		return activity;
	}
	/**
	 * setters for the fields of the class
	 * @param the field as string
	 */
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}

	public void setEnd_time(String end_time) {
		this.end_time = end_time;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}
	
	@Override
	public String toString() {
		return "Activity:	start time:	" + start_time + ", end time:	" + end_time + "	activity:	" + activity ;
	}
	
}
