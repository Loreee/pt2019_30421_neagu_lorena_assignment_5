package BussinessLayer;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import DataLayer.MonitoredData;

import java.text.SimpleDateFormat;

/**
 * defines the methods that implement each task
 * @author LORENA NEAGU
 *
 */
public class DataProcessing {
	/**
	 * retrieves the data from the log file
	 * @return list of MonitoredData objects retrieved from the log file representing each entry
	 * @throws IOException
	 */
	public List<MonitoredData> retrieveData() throws IOException{
		return Files.lines(Paths.get("Activities.txt"))
				.map(string -> string.split("\t\t"))
				.map(string -> {return new MonitoredData(string[0].trim(), string[1].trim(), string[2].trim());
				})
				.collect(Collectors.toList());
	}
	
	/**
	 * counts the days of monitored data that appear in the log file
	 * @param datalist
	 * @return
	 */
	public int countDays(List<MonitoredData> datalist) {
		Date minDate = datalist.stream()
				.map(MonitoredData::getStart_time)
				.map(string -> {
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					try {
						return format.parse(string);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						throw new IllegalArgumentException("");
					}
				}).min(Date::compareTo).orElse(null);
		
		Date maxDate = datalist.stream()
				.map(MonitoredData::getEnd_time)
				.map(string -> {
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					try {
						return format.parse(string);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						throw new IllegalArgumentException("");
					}
				}).max(Date::compareTo).orElse(null);
		
		return (int)(maxDate.getTime() - minDate.getTime()) / (1000 * 60 * 60 * 24);
	}
	
	/**
	 * counts the times each activity has appeared over the monitoring period
	 * 
	 * @param datalist
	 * @return map of type <String,Integer> representing the mapping of activities to their count
	 */
	public Map<String,Integer> countActivities(List<MonitoredData> datalist) {
		return datalist.stream()
				.map(MonitoredData::getActivity)
				.collect(Collectors.groupingBy(string -> {
					return string;
				}
				, Collectors.summingInt(activity -> {
					return 1;
				})));
	}
	
	/**
	 * counts how many times each activity has appeared for each day over the monitoring period
	 * @param datalist
	 * @return a map containing for each day how many times an activity happened
	 */
	public Map<Integer,Map<String,Integer>> countActivitiesEachDay(List<MonitoredData> datalist){
		
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return datalist.stream()
				.collect(Collectors.groupingBy(data -> {
					try {
						calendar.setTime(format.parse(data.getEnd_time()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
				}
			return calendar.get(Calendar.DAY_OF_MONTH);
		}, Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingInt(lore -> 1))));
	}
	
	/**
	 * For each line from the file maps for the activity label the duration recorded on that line
	 * @param datalist
	 * @return a map containing for each line the duration of the activity
	 */
	public Map<Integer, Map<String,Integer>> computeDurations(List<MonitoredData> datalist){
		Map<Integer, Map<String,Integer>> durations = new HashMap();
		AtomicInteger i = new AtomicInteger();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		datalist.forEach(string -> {
			Map<String,Integer> map = new HashMap();
			try {
				map.put(string.getActivity(), (int)(format.parse(string.getEnd_time()).getTime() - format.parse(string.getStart_time()).getTime()) / 6000);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			durations.put(i.getAndIncrement(),map);
		});
			return durations;
	}
	
	/**
	 * computes the duration over the monitoring period of each activity
	 * @param datalist
	 * @return a map containing the duration for each activity
	 */
	public Map<String,Long> compute(List<MonitoredData> datalist){
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		return datalist.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingLong(lore -> {
			try {
				return (format.parse(lore.getEnd_time()).getTime() - format.parse(lore.getStart_time()).getTime()) / 6000; 
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new IllegalArgumentException("");
			}
		})));
		
	}
	/**
	 * Filter the activities that have 90% of the monitoring records with duration less than 5 minutes
	 * @param datalist
	 * @return a list of activities
	 */
	public List<String> filter(List<MonitoredData> datalist){
		int time = 5 * 60;
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Map<String,Integer> map = datalist.stream().collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.summingInt(lore -> {
			try {
				if( (int)(format.parse(lore.getEnd_time()).getTime() - format.parse(lore.getStart_time()).getTime()) / 1000  < time )
					return 1;
				else return 0;
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new IllegalArgumentException("");
			}
		})));
		
		Map<String,Integer> mapp = countActivities(datalist);
		
		return datalist.stream().filter(lore -> map.get(lore.getActivity()) >= mapp.get(lore.getActivity()) * 9 / 10).map(MonitoredData::getActivity).distinct().collect(Collectors.toList());
	}
}
