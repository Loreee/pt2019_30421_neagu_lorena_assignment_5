package Start;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import BussinessLayer.DataProcessing;
import DataLayer.MonitoredData;
import PresentationLayer.PrintData;
/**
 * class that implements the main program
 * 
 * @author LORENA NEAGU
 *
 */
public class Main {

	public static void main(String[] args) throws IOException {
		DataProcessing dp = new DataProcessing();
		List<MonitoredData> datalist = dp.retrieveData();
		
		for(Iterator<MonitoredData> it = datalist.iterator(); it.hasNext();) {
			MonitoredData md = it.next();
			System.out.println(md);
		}
		System.out.println();
		int numberOfMonitoredDays = dp.countDays(datalist);
		PrintData.printMonitoredDays(numberOfMonitoredDays);
		System.out.println();
		PrintData.printCountActivities(dp.countActivities(datalist));
		System.out.println();
		PrintData.printCountActivitiesEachDay(dp.countActivitiesEachDay(datalist));
		System.out.println();
		PrintData.printComputeDurations(dp.computeDurations(datalist));
		System.out.println();
		PrintData.printCompute(dp.compute(datalist));
		System.out.println();
		PrintData.printFilter(dp.filter(datalist));
	}

}
